<%@ include file="header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div tiles:fragment="content" class="col-md-4 col-md-offset-4 centered">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  <title>Jira Visualisation</title>
  <table class="indextable" class="box">
    <tr>
      <th><font size="10" face="helvetica">View Data</font></th>
      <th><font size="10" face="helvetica">Load Data</font></th>
    </tr>
    <tr>
      <td><a href="http://localhost:5601/app/kibana"><img src="assets/images/kibanalogo.png"/></a></td>
      <td><a href="jiralogin.jsp"><img src="assets/images/eslogo.png"/></a></td>
    </tr>
  </table>
</div>