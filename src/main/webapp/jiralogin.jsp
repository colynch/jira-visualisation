<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="th" uri="http://www.springframework.org/tags/form" %>
<%@ include file="header.jsp" %>
<div tiles:fragment="content" class="col-md-4 col-md-offset-4 centered">

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/main.css">
    <title>Jira Visualisation</title>
    <div class="spacer100"></div>
    <div class="row" align="center">

        <form name="f" th:action="@{/login}" method="post" class="form-horizontal login-form">
            <fieldset align="center">

                <!-- Form Name -->
                <div>
                    <font size="5" align="center" face="helvetica">Please complete the following for your project:</font>
                </div>

                <div class="form-group">
                    <label class="col-md-5 control-label" for="projectname"><font size="3" face="helvetica">Jira Project Name</font></label>

                    <div class="col-md-5">
                        <input id="projectname" name="projectname" type="text" placeholder="Project Name"
                               class="form-control input-md" required="">
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-5 control-label" for="username"><font size="3" face="helvetica">Username</font></label>

                    <div class="col-md-5">
                        <input id="username" name="username" type="text" placeholder="Username"
                               class="form-control input-md" required="">
                    </div>
                </div>

                <!-- Password input-->
                <div class="form-group">
                    <label class="col-md-5 control-label" for="password"><font size="3" face="helvetica">Password</font></label>

                    <div class="col-md-5">
                        <input id="password" name="password" type="password" placeholder="Password"
                               class="form-control input-md" required="">
                    </div>
                </div>

                <div class="form-actions text-center">
                    <button type="submit" class="btn">Submit</button>
                </div>

            </fieldset>
        </form>
    </div>
</div>

