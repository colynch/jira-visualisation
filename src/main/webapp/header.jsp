<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/x-icon" href="${pageContext.request.contextPath}/assets/icons/favicon.ico">

    <title>Jira Visualisation</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="http://js.arcgis.com/3.11/esri/css/esri.css">

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,700" type="text/css">
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/dojo/1.8.7/dijit/themes/claro/claro.css" media="screen">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/compiled/main.css">
</head>
<header class="main-header" role="banner">
</header>

