package com.jira.elasticsearch.service;

public interface UserService {

    String buildAuthorisationString(String username, String password);

}
