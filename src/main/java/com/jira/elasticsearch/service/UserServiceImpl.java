package com.jira.elasticsearch.service;

import com.sun.org.apache.xml.internal.security.utils.Base64;
import org.springframework.stereotype.Component;

@Component
public class UserServiceImpl implements UserService {

    public String buildAuthorisationString(String username, String password){
        String credentials = username + ":" + password;
        return "Basic " + new String(Base64.encode(credentials.getBytes(), Base64.BASE64DEFAULTLENGTH));
    }
}
