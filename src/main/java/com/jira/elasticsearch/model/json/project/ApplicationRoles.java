package com.jira.elasticsearch.model.json.project;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "size",
        "max-results",
        "items"
})
public class ApplicationRoles {

    /**
     *
     * (Required)
     *
     */
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("max-results")
    private Integer maxResults;
    @JsonProperty("items")
    private List<Item> items = new ArrayList<Item>();

    /**
     *
     * (Required)
     *
     * @return
     * The size
     */
    @JsonProperty("size")
    public Integer getSize() {
        return size;
    }

    /**
     *
     * (Required)
     *
     * @param size
     * The size
     */
    @JsonProperty("size")
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     *
     * @return
     * The maxResults
     */
    @JsonProperty("max-results")
    public Integer getMaxResults() {
        return maxResults;
    }

    /**
     *
     * @param maxResults
     * The max-results
     */
    @JsonProperty("max-results")
    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    /**
     *
     * @return
     * The items
     */
    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    /**
     *
     * @param items
     * The items
     */
    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

}