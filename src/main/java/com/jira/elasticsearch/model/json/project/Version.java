package com.jira.elasticsearch.model.json.project;


import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Version
 * <p>
 *
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "expand",
        "self",
        "id",
        "description",
        "name",
        "archived",
        "released",
        "overdue",
        "userStartDate",
        "userReleaseDate",
        "project",
        "projectId",
        "moveUnfixedIssuesTo",
        "operations",
        "remotelinks"
})
public class Version {

    @JsonProperty("expand")
    private String expand;
    @JsonProperty("self")
    private URI self;
    @JsonProperty("id")
    private String id;
    @JsonProperty("description")
    private String description;
    @JsonProperty("name")
    private String name;
    @JsonProperty("archived")
    private Boolean archived;
    @JsonProperty("released")
    private Boolean released;
    @JsonProperty("overdue")
    private Boolean overdue;
    @JsonProperty("userStartDate")
    private String userStartDate;
    @JsonProperty("userReleaseDate")
    private String userReleaseDate;
    @JsonProperty("project")
    private String project;
    @JsonProperty("projectId")
    private Integer projectId;
    @JsonProperty("moveUnfixedIssuesTo")
    private URI moveUnfixedIssuesTo;
    @JsonProperty("operations")
    private List<Operation> operations = new ArrayList<Operation>();
    @JsonProperty("remotelinks")
    private List<Remotelink> remotelinks = new ArrayList<Remotelink>();

    /**
     *
     * @return
     * The expand
     */
    @JsonProperty("expand")
    public String getExpand() {
        return expand;
    }

    /**
     *
     * @param expand
     * The expand
     */
    @JsonProperty("expand")
    public void setExpand(String expand) {
        this.expand = expand;
    }

    /**
     *
     * @return
     * The self
     */
    @JsonProperty("self")
    public URI getSelf() {
        return self;
    }

    /**
     *
     * @param self
     * The self
     */
    @JsonProperty("self")
    public void setSelf(URI self) {
        this.self = self;
    }

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The archived
     */
    @JsonProperty("archived")
    public Boolean getArchived() {
        return archived;
    }

    /**
     *
     * @param archived
     * The archived
     */
    @JsonProperty("archived")
    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    /**
     *
     * @return
     * The released
     */
    @JsonProperty("released")
    public Boolean getReleased() {
        return released;
    }

    /**
     *
     * @param released
     * The released
     */
    @JsonProperty("released")
    public void setReleased(Boolean released) {
        this.released = released;
    }

    /**
     *
     * @return
     * The overdue
     */
    @JsonProperty("overdue")
    public Boolean getOverdue() {
        return overdue;
    }

    /**
     *
     * @param overdue
     * The overdue
     */
    @JsonProperty("overdue")
    public void setOverdue(Boolean overdue) {
        this.overdue = overdue;
    }

    /**
     *
     * @return
     * The userStartDate
     */
    @JsonProperty("userStartDate")
    public String getUserStartDate() {
        return userStartDate;
    }

    /**
     *
     * @param userStartDate
     * The userStartDate
     */
    @JsonProperty("userStartDate")
    public void setUserStartDate(String userStartDate) {
        this.userStartDate = userStartDate;
    }

    /**
     *
     * @return
     * The userReleaseDate
     */
    @JsonProperty("userReleaseDate")
    public String getUserReleaseDate() {
        return userReleaseDate;
    }

    /**
     *
     * @param userReleaseDate
     * The userReleaseDate
     */
    @JsonProperty("userReleaseDate")
    public void setUserReleaseDate(String userReleaseDate) {
        this.userReleaseDate = userReleaseDate;
    }

    /**
     *
     * @return
     * The project
     */
    @JsonProperty("project")
    public String getProject() {
        return project;
    }

    /**
     *
     * @param project
     * The project
     */
    @JsonProperty("project")
    public void setProject(String project) {
        this.project = project;
    }

    /**
     *
     * @return
     * The projectId
     */
    @JsonProperty("projectId")
    public Integer getProjectId() {
        return projectId;
    }

    /**
     *
     * @param projectId
     * The projectId
     */
    @JsonProperty("projectId")
    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    /**
     *
     * @return
     * The moveUnfixedIssuesTo
     */
    @JsonProperty("moveUnfixedIssuesTo")
    public URI getMoveUnfixedIssuesTo() {
        return moveUnfixedIssuesTo;
    }

    /**
     *
     * @param moveUnfixedIssuesTo
     * The moveUnfixedIssuesTo
     */
    @JsonProperty("moveUnfixedIssuesTo")
    public void setMoveUnfixedIssuesTo(URI moveUnfixedIssuesTo) {
        this.moveUnfixedIssuesTo = moveUnfixedIssuesTo;
    }

    /**
     *
     * @return
     * The operations
     */
    @JsonProperty("operations")
    public List<Operation> getOperations() {
        return operations;
    }

    /**
     *
     * @param operations
     * The operations
     */
    @JsonProperty("operations")
    public void setOperations(List<Operation> operations) {
        this.operations = operations;
    }

    /**
     *
     * @return
     * The remotelinks
     */
    @JsonProperty("remotelinks")
    public List<Remotelink> getRemotelinks() {
        return remotelinks;
    }

    /**
     *
     * @param remotelinks
     * The remotelinks
     */
    @JsonProperty("remotelinks")
    public void setRemotelinks(List<Remotelink> remotelinks) {
        this.remotelinks = remotelinks;
    }

}