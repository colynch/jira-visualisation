package com.jira.elasticsearch.model.json.project;

import java.net.URI;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Remote Entity Link
 * <p>
 *
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "self",
        "name",
        "link"
})
public class Remotelink {

    @JsonProperty("self")
    private URI self;
    @JsonProperty("name")
    private String name;
    @JsonProperty("link")
    private Object link;

    /**
     *
     * @return
     * The self
     */
    @JsonProperty("self")
    public URI getSelf() {
        return self;
    }

    /**
     *
     * @param self
     * The self
     */
    @JsonProperty("self")
    public void setSelf(URI self) {
        this.self = self;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The link
     */
    @JsonProperty("link")
    public Object getLink() {
        return link;
    }

    /**
     *
     * @param link
     * The link
     */
    @JsonProperty("link")
    public void setLink(Object link) {
        this.link = link;
    }

}
