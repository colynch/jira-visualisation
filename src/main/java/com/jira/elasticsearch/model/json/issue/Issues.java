package com.jira.elasticsearch.model.json.issue;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "score",
        "type",
        "expand",
        "issues"
})
public class Issues {

    @JsonProperty("id")
    private String id;
    @JsonProperty("index")
    private String index;
    @JsonProperty("score")
    private String score;
    @JsonProperty("expand")
    private String[] expand;
    @JsonProperty("issues")
    private Issue[] issues;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("index")
    public String getIndex() {
        return index;
    }

    @JsonProperty("index")
    public void setIndex(String index) {
        this.index = index;
    }

    @JsonProperty("score")
    public String getScore() {
        return score;
    }

    @JsonProperty("score")
    public void setScore(String score) {
        this.score = score;
    }

    @JsonProperty("expand")
    public String[] getExpand() {
        return expand;
    }

    @JsonProperty("expand")
    public void setExpand(String[] expand) {
        this.expand = expand;
    }

    @JsonProperty("issues")
    public Issue[] getIssues() {
        return issues;
    }

    @JsonProperty("issues")
    public void setIssues(Issue[] issues) {
        this.issues = issues;
    }
}
