package com.jira.elasticsearch.model.json.project;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Simple Link
 * <p>
 *
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "id",
        "styleClass",
        "iconClass",
        "label",
        "title",
        "href",
        "weight"
})
public class Operation {

    @JsonProperty("id")
    private String id;
    @JsonProperty("styleClass")
    private String styleClass;
    @JsonProperty("iconClass")
    private String iconClass;
    @JsonProperty("label")
    private String label;
    @JsonProperty("title")
    private String title;
    @JsonProperty("href")
    private String href;
    @JsonProperty("weight")
    private Integer weight;

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The styleClass
     */
    @JsonProperty("styleClass")
    public String getStyleClass() {
        return styleClass;
    }

    /**
     *
     * @param styleClass
     * The styleClass
     */
    @JsonProperty("styleClass")
    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    /**
     *
     * @return
     * The iconClass
     */
    @JsonProperty("iconClass")
    public String getIconClass() {
        return iconClass;
    }

    /**
     *
     * @param iconClass
     * The iconClass
     */
    @JsonProperty("iconClass")
    public void setIconClass(String iconClass) {
        this.iconClass = iconClass;
    }

    /**
     *
     * @return
     * The label
     */
    @JsonProperty("label")
    public String getLabel() {
        return label;
    }

    /**
     *
     * @param label
     * The label
     */
    @JsonProperty("label")
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     *
     * @return
     * The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     *
     * @return
     * The href
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     *
     * @param href
     * The href
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     *
     * @return
     * The weight
     */
    @JsonProperty("weight")
    public Integer getWeight() {
        return weight;
    }

    /**
     *
     * @param weight
     * The weight
     */
    @JsonProperty("weight")
    public void setWeight(Integer weight) {
        this.weight = weight;
    }

}
