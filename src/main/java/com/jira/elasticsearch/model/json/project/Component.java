package com.jira.elasticsearch.model.json.project;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Component
 * <p>
 *
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "self",
        "id",
        "name",
        "description",
        "lead",
        "leadUserName",
        "assigneeType",
        "assignee",
        "realAssigneeType",
        "realAssignee",
        "isAssigneeTypeValid",
        "project",
        "projectId"
})
public class Component {

    @JsonProperty("self")
    private URI self;
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    /**
     * User
     * <p>
     *
     *
     */
    @JsonProperty("lead")
    private Lead lead;
    @JsonProperty("leadUserName")
    private String leadUserName;
    @JsonProperty("assigneeType")
    private Component.AssigneeType assigneeType;
    /**
     * User
     * <p>
     *
     *
     */
    @JsonProperty("assignee")
    private Assignee assignee;
    @JsonProperty("realAssigneeType")
    private Component.RealAssigneeType realAssigneeType;
    /**
     * User
     * <p>
     *
     *
     */
    @JsonProperty("realAssignee")
    private RealAssignee realAssignee;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("isAssigneeTypeValid")
    private Boolean isAssigneeTypeValid;
    @JsonProperty("project")
    private String project;
    @JsonProperty("projectId")
    private Integer projectId;

    /**
     *
     * @return
     * The self
     */
    @JsonProperty("self")
    public URI getSelf() {
        return self;
    }

    /**
     *
     * @param self
     * The self
     */
    @JsonProperty("self")
    public void setSelf(URI self) {
        this.self = self;
    }

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * User
     * <p>
     *
     *
     * @return
     * The lead
     */
    @JsonProperty("lead")
    public Lead getLead() {
        return lead;
    }

    /**
     * User
     * <p>
     *
     *
     * @param lead
     * The lead
     */
    @JsonProperty("lead")
    public void setLead(Lead lead) {
        this.lead = lead;
    }

    /**
     *
     * @return
     * The leadUserName
     */
    @JsonProperty("leadUserName")
    public String getLeadUserName() {
        return leadUserName;
    }

    /**
     *
     * @param leadUserName
     * The leadUserName
     */
    @JsonProperty("leadUserName")
    public void setLeadUserName(String leadUserName) {
        this.leadUserName = leadUserName;
    }

    /**
     *
     * @return
     * The assigneeType
     */
    @JsonProperty("assigneeType")
    public Component.AssigneeType getAssigneeType() {
        return assigneeType;
    }

    /**
     *
     * @param assigneeType
     * The assigneeType
     */
    @JsonProperty("assigneeType")
    public void setAssigneeType(Component.AssigneeType assigneeType) {
        this.assigneeType = assigneeType;
    }

    /**
     * User
     * <p>
     *
     *
     * @return
     * The assignee
     */
    @JsonProperty("assignee")
    public Assignee getAssignee() {
        return assignee;
    }

    /**
     * User
     * <p>
     *
     *
     * @param assignee
     * The assignee
     */
    @JsonProperty("assignee")
    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    /**
     *
     * @return
     * The realAssigneeType
     */
    @JsonProperty("realAssigneeType")
    public Component.RealAssigneeType getRealAssigneeType() {
        return realAssigneeType;
    }

    /**
     *
     * @param realAssigneeType
     * The realAssigneeType
     */
    @JsonProperty("realAssigneeType")
    public void setRealAssigneeType(Component.RealAssigneeType realAssigneeType) {
        this.realAssigneeType = realAssigneeType;
    }

    /**
     * User
     * <p>
     *
     *
     * @return
     * The realAssignee
     */
    @JsonProperty("realAssignee")
    public RealAssignee getRealAssignee() {
        return realAssignee;
    }

    /**
     * User
     * <p>
     *
     *
     * @param realAssignee
     * The realAssignee
     */
    @JsonProperty("realAssignee")
    public void setRealAssignee(RealAssignee realAssignee) {
        this.realAssignee = realAssignee;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The isAssigneeTypeValid
     */
    @JsonProperty("isAssigneeTypeValid")
    public Boolean getIsAssigneeTypeValid() {
        return isAssigneeTypeValid;
    }

    /**
     *
     * (Required)
     *
     * @param isAssigneeTypeValid
     * The isAssigneeTypeValid
     */
    @JsonProperty("isAssigneeTypeValid")
    public void setIsAssigneeTypeValid(Boolean isAssigneeTypeValid) {
        this.isAssigneeTypeValid = isAssigneeTypeValid;
    }

    /**
     *
     * @return
     * The project
     */
    @JsonProperty("project")
    public String getProject() {
        return project;
    }

    /**
     *
     * @param project
     * The project
     */
    @JsonProperty("project")
    public void setProject(String project) {
        this.project = project;
    }

    /**
     *
     * @return
     * The projectId
     */
    @JsonProperty("projectId")
    public Integer getProjectId() {
        return projectId;
    }

    /**
     *
     * @param projectId
     * The projectId
     */
    @JsonProperty("projectId")
    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    @Generated("org.jsonschema2pojo")
    public enum AssigneeType {

        PROJECT_DEFAULT("PROJECT_DEFAULT"),
        COMPONENT_LEAD("COMPONENT_LEAD"),
        PROJECT_LEAD("PROJECT_LEAD"),
        UNASSIGNED("UNASSIGNED");
        private final String value;
        private final static Map<String, Component.AssigneeType> CONSTANTS = new HashMap<String, Component.AssigneeType>();

        static {
            for (Component.AssigneeType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private AssigneeType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static Component.AssigneeType fromValue(String value) {
            Component.AssigneeType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    @Generated("org.jsonschema2pojo")
    public enum RealAssigneeType {

        PROJECT_DEFAULT("PROJECT_DEFAULT"),
        COMPONENT_LEAD("COMPONENT_LEAD"),
        PROJECT_LEAD("PROJECT_LEAD"),
        UNASSIGNED("UNASSIGNED");
        private final String value;
        private final static Map<String, Component.RealAssigneeType> CONSTANTS = new HashMap<String, Component.RealAssigneeType>();

        static {
            for (Component.RealAssigneeType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private RealAssigneeType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static Component.RealAssigneeType fromValue(String value) {
            Component.RealAssigneeType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
