package com.jira.elasticsearch.model.json.project;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Project
 * <p>
 *
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "expand",
        "self",
        "id",
        "key",
        "description",
        "lead",
        "components",
        "issueTypes",
        "url",
        "email",
        "assigneeType",
        "versions",
        "name",
        "roles",
        "avatarUrls",
        "projectKeys",
        "projectCategory",
        "projectTypeKey"
})
public class Project {

    @JsonProperty("expand")
    private String expand;
    @JsonProperty("self")
    private URI self;
    @JsonProperty("id")
    private String id;
    @JsonProperty("key")
    private String key;
    @JsonProperty("description")
    private String description;
    /**
     * User
     * <p>
     *
     *
     */
    @JsonProperty("lead")
    private Lead lead;
    @JsonProperty("components")
    private List<Component> components = new ArrayList<Component>();
    @JsonProperty("issueTypes")
    private List<IssueType> issueTypes = new ArrayList<IssueType>();
    @JsonProperty("url")
    private String url;
    @JsonProperty("email")
    private String email;
    @JsonProperty("assigneeType")
    private Project.AssigneeType assigneeType;
    @JsonProperty("versions")
    private List<Version> versions = new ArrayList<Version>();
    @JsonProperty("name")
    private String name;
    @JsonProperty("roles")
    private Roles roles;
    @JsonProperty("avatarUrls")
    private AvatarUrls avatarUrls;
    @JsonProperty("projectKeys")
    private List<String> projectKeys = new ArrayList<String>();
    /**
     * Project Category
     * <p>
     *
     *
     */
    @JsonProperty("projectCategory")
    private ProjectCategory projectCategory;
    @JsonProperty("projectTypeKey")
    private String projectTypeKey;

    /**
     *
     * @return
     * The expand
     */
    @JsonProperty("expand")
    public String getExpand() {
        return expand;
    }

    /**
     *
     * @param expand
     * The expand
     */
    @JsonProperty("expand")
    public void setExpand(String expand) {
        this.expand = expand;
    }

    /**
     *
     * @return
     * The self
     */
    @JsonProperty("self")
    public URI getSelf() {
        return self;
    }

    /**
     *
     * @param self
     * The self
     */
    @JsonProperty("self")
    public void setSelf(URI self) {
        this.self = self;
    }

    /**
     *
     * @return
     * The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The key
     */
    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key
     * The key
     */
    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @return
     * The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * User
     * <p>
     *
     *
     * @return
     * The lead
     */
    @JsonProperty("lead")
    public Lead getLead() {
        return lead;
    }

    /**
     * User
     * <p>
     *
     *
     * @param lead
     * The lead
     */
    @JsonProperty("lead")
    public void setLead(Lead lead) {
        this.lead = lead;
    }

    /**
     *
     * @return
     * The components
     */
    @JsonProperty("components")
    public List<Component> getComponents() {
        return components;
    }

    /**
     *
     * @param components
     * The components
     */
    @JsonProperty("components")
    public void setComponents(List<Component> components) {
        this.components = components;
    }

    /**
     *
     * @return
     * The issueTypes
     */
    @JsonProperty("issueTypes")
    public List<IssueType> getIssueTypes() {
        return issueTypes;
    }

    /**
     *
     * @param issueTypes
     * The issueTypes
     */
    @JsonProperty("issueTypes")
    public void setIssueTypes(List<IssueType> issueTypes) {
        this.issueTypes = issueTypes;
    }

    /**
     *
     * @return
     * The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     * The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The assigneeType
     */
    @JsonProperty("assigneeType")
    public Project.AssigneeType getAssigneeType() {
        return assigneeType;
    }

    /**
     *
     * @param assigneeType
     * The assigneeType
     */
    @JsonProperty("assigneeType")
    public void setAssigneeType(Project.AssigneeType assigneeType) {
        this.assigneeType = assigneeType;
    }

    /**
     *
     * @return
     * The versions
     */
    @JsonProperty("versions")
    public List<Version> getVersions() {
        return versions;
    }

    /**
     *
     * @param versions
     * The versions
     */
    @JsonProperty("versions")
    public void setVersions(List<Version> versions) {
        this.versions = versions;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The roles
     */
    @JsonProperty("roles")
    public Roles getRoles() {
        return roles;
    }

    /**
     *
     * @param roles
     * The roles
     */
    @JsonProperty("roles")
    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    /**
     *
     * @return
     * The avatarUrls
     */
    @JsonProperty("avatarUrls")
    public AvatarUrls getAvatarUrls() {
        return avatarUrls;
    }

    /**
     *
     * @param avatarUrls
     * The avatarUrls
     */
    @JsonProperty("avatarUrls")
    public void setAvatarUrls(AvatarUrls avatarUrls) {
        this.avatarUrls = avatarUrls;
    }

    /**
     *
     * @return
     * The projectKeys
     */
    @JsonProperty("projectKeys")
    public List<String> getProjectKeys() {
        return projectKeys;
    }

    /**
     *
     * @param projectKeys
     * The projectKeys
     */
    @JsonProperty("projectKeys")
    public void setProjectKeys(List<String> projectKeys) {
        this.projectKeys = projectKeys;
    }

    /**
     * Project Category
     * <p>
     *
     *
     * @return
     * The projectCategory
     */
    @JsonProperty("projectCategory")
    public ProjectCategory getProjectCategory() {
        return projectCategory;
    }

    /**
     * Project Category
     * <p>
     *
     *
     * @param projectCategory
     * The projectCategory
     */
    @JsonProperty("projectCategory")
    public void setProjectCategory(ProjectCategory projectCategory) {
        this.projectCategory = projectCategory;
    }

    /**
     *
     * @return
     * The projectTypeKey
     */
    @JsonProperty("projectTypeKey")
    public String getProjectTypeKey() {
        return projectTypeKey;
    }

    /**
     *
     * @param projectTypeKey
     * The projectTypeKey
     */
    @JsonProperty("projectTypeKey")
    public void setProjectTypeKey(String projectTypeKey) {
        this.projectTypeKey = projectTypeKey;
    }

    @Generated("org.jsonschema2pojo")
    public enum AssigneeType {

        PROJECT_LEAD("PROJECT_LEAD"),
        UNASSIGNED("UNASSIGNED");
        private final String value;
        private final static Map<String, Project.AssigneeType> CONSTANTS = new HashMap<String, Project.AssigneeType>();

        static {
            for (Project.AssigneeType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private AssigneeType(String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public String toString() {
            return this.value;
        }

        @JsonCreator
        public static Project.AssigneeType fromValue(String value) {
            Project.AssigneeType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}