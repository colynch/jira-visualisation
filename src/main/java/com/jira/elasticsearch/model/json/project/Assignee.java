package com.jira.elasticsearch.model.json.project;

import java.net.URI;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * User
 * <p>
 *
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
        "self",
        "key",
        "name",
        "emailAddress",
        "avatarUrls",
        "displayName",
        "active",
        "timeZone",
        "locale",
        "groups",
        "applicationRoles",
        "expand"
})
public class Assignee {

    @JsonProperty("self")
    private URI self;
    @JsonProperty("key")
    private String key;
    @JsonProperty("name")
    private String name;
    @JsonProperty("emailAddress")
    private String emailAddress;
    @JsonProperty("avatarUrls")
    private AvatarUrls avatarUrls;
    @JsonProperty("displayName")
    private String displayName;
    /**
     *
     * (Required)
     *
     */
    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("timeZone")
    private String timeZone;
    @JsonProperty("locale")
    private String locale;
    /**
     * Simple List Wrapper
     * <p>
     *
     *
     */
    @JsonProperty("groups")
    private Groups groups;
    /**
     * Simple List Wrapper
     * <p>
     *
     *
     */
    @JsonProperty("applicationRoles")
    private ApplicationRoles applicationRoles;
    @JsonProperty("expand")
    private String expand;

    /**
     *
     * @return
     * The self
     */
    @JsonProperty("self")
    public URI getSelf() {
        return self;
    }

    /**
     *
     * @param self
     * The self
     */
    @JsonProperty("self")
    public void setSelf(URI self) {
        this.self = self;
    }

    /**
     *
     * @return
     * The key
     */
    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    /**
     *
     * @param key
     * The key
     */
    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    /**
     *
     * @return
     * The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The emailAddress
     */
    @JsonProperty("emailAddress")
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     *
     * @param emailAddress
     * The emailAddress
     */
    @JsonProperty("emailAddress")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     *
     * @return
     * The avatarUrls
     */
    @JsonProperty("avatarUrls")
    public AvatarUrls getAvatarUrls() {
        return avatarUrls;
    }

    /**
     *
     * @param avatarUrls
     * The avatarUrls
     */
    @JsonProperty("avatarUrls")
    public void setAvatarUrls(AvatarUrls avatarUrls) {
        this.avatarUrls = avatarUrls;
    }

    /**
     *
     * @return
     * The displayName
     */
    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    /**
     *
     * @param displayName
     * The displayName
     */
    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *
     * (Required)
     *
     * @return
     * The active
     */
    @JsonProperty("active")
    public Boolean getActive() {
        return active;
    }

    /**
     *
     * (Required)
     *
     * @param active
     * The active
     */
    @JsonProperty("active")
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     *
     * @return
     * The timeZone
     */
    @JsonProperty("timeZone")
    public String getTimeZone() {
        return timeZone;
    }

    /**
     *
     * @param timeZone
     * The timeZone
     */
    @JsonProperty("timeZone")
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    /**
     *
     * @return
     * The locale
     */
    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    /**
     *
     * @param locale
     * The locale
     */
    @JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

    /**
     * Simple List Wrapper
     * <p>
     *
     *
     * @return
     * The groups
     */
    @JsonProperty("groups")
    public Groups getGroups() {
        return groups;
    }

    /**
     * Simple List Wrapper
     * <p>
     *
     *
     * @param groups
     * The groups
     */
    @JsonProperty("groups")
    public void setGroups(Groups groups) {
        this.groups = groups;
    }

    /**
     * Simple List Wrapper
     * <p>
     *
     *
     * @return
     * The applicationRoles
     */
    @JsonProperty("applicationRoles")
    public ApplicationRoles getApplicationRoles() {
        return applicationRoles;
    }

    /**
     * Simple List Wrapper
     * <p>
     *
     *
     * @param applicationRoles
     * The applicationRoles
     */
    @JsonProperty("applicationRoles")
    public void setApplicationRoles(ApplicationRoles applicationRoles) {
        this.applicationRoles = applicationRoles;
    }

    /**
     *
     * @return
     * The expand
     */
    @JsonProperty("expand")
    public String getExpand() {
        return expand;
    }

    /**
     *
     * @param expand
     * The expand
     */
    @JsonProperty("expand")
    public void setExpand(String expand) {
        this.expand = expand;
    }

}
