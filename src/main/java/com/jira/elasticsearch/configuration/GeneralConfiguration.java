package com.jira.elasticsearch.configuration;

import com.jira.elasticsearch.dao.ElasticsearchDao;
import com.jira.elasticsearch.dao.ElasticsearchDaoImpl;
import com.jira.elasticsearch.dao.JiraDao;
import com.jira.elasticsearch.dao.JiraDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("com.jira.elasticsearch")
public class GeneralConfiguration {

}
