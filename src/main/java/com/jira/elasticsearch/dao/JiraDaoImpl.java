package com.jira.elasticsearch.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jira.elasticsearch.model.json.issue.Issue;
import com.jira.elasticsearch.model.json.issue.Issues;
import com.jira.elasticsearch.service.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.google.gson.*;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.jira.elasticsearch.utils.JiraConstants.*;

@Component
public class JiraDaoImpl implements JiraDao {

    @Autowired
    private UserServiceImpl userService;

    private static final Logger LOGGER = LoggerFactory.getLogger(JiraDaoImpl.class);

    private String getJsonFromJiraApi(String jql, String username, String password) throws IOException, JAXBException{
        URL url = new URL(JIRA_URL + jql);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        String userCredentials = userService.buildAuthorisationString(username, password);
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Authorization", userCredentials);

        InputStream json = connection.getInputStream();
        String jsonString = getStringFromInputStream(json);
        json.close();
        return jsonString;
    }

    public String getSingleIssueAsJson(String issueId, String username, String password) throws IOException, JAXBException{
        return getJsonFromJiraApi(issueId, username, password);
    }

    public String getAllProjectIssuesAsSingleJson(String jql, String username, String password) throws IOException, JAXBException{
        return getJsonFromJiraApi(jql, username, password);
    }

    public String[] getAllProjectIssuesAsJsonArray(String jql, String username, String password) throws IOException, JAXBException{

        Issue[] issuesArray = getListOfIssuesAsArray(jql, username, password);
        return convertIssueArrayToJsonArray(issuesArray);
    }

    public Issue getIssueAsObject(String issueId, String username, String password) throws IOException, JAXBException{
        String jsonString = getSingleIssueAsJson(issueId, username, password);

        Gson gson = new Gson();
        Issue issue = gson.fromJson(jsonString, Issue.class);
        return issue;
    }

    public Issue[] getListOfIssuesAsArray(String jql, String username, String password) throws IOException, JAXBException{

        String jsonString = getAllProjectIssuesAsSingleJson(jql, username, password);
        Gson gson = new Gson();
        Issues issues = gson.fromJson(jsonString, Issues.class);

        Issue[] issueArray = issues.getIssues();
        return issueArray;
    }

    public String[] convertIssueArrayToJsonArray(Issue[] issues) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();
        String[] json = new String[issues.length];
        int i = 0;
        for (Issue issue: issues){
            json[i]  = mapper.writeValueAsString(issue);
            i++;
        }
        return json;
    }

    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

}
