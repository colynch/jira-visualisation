package com.jira.elasticsearch.dao;

import java.io.IOException;
import java.util.List;

public interface ElasticsearchDao {

    void loadStringAsDocument(String json) throws IOException;
    void loadMultipleDocuments(String[] json) throws IOException;
}
