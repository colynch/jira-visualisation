package com.jira.elasticsearch.dao;

import com.jira.elasticsearch.model.json.issue.Issue;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface JiraDao  {

    Issue getIssueAsObject(String issueId, String username, String password) throws IOException, JAXBException;
    Issue[] getListOfIssuesAsArray(String jql, String username, String password) throws IOException, JAXBException;
    String getSingleIssueAsJson(String issueId, String username, String password) throws IOException, JAXBException;
}
