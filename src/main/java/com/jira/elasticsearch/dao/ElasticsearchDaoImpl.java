package com.jira.elasticsearch.dao;

import com.jira.elasticsearch.model.json.issue.Issue;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.SimpleQueryParser;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetAddress;

@Component
public class ElasticsearchDaoImpl implements ElasticsearchDao {

    private static final String INDEX = "test_data";
    private static final String TYPE = "issue";
    private static final String HOST = "localhost";
    private static final int PORT = 9300;

    public void loadStringAsDocument(String json) throws IOException{
        Settings settings = Settings.settingsBuilder()
                .put("client.transport.ping_timeout", "15s")
                .put("client.transport.sniff", false)
                .put("client.transport.nodes_sampler_interval", "15s")
                .put("cluster.name", "jira-cluster")
                .build();

        Client client = TransportClient.builder().settings(settings).build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(HOST),PORT));

        IndexResponse response = client.prepareIndex(INDEX, TYPE)
                .setSource(json)
                .get();
    }

    public void loadMultipleDocuments(String[] jsonDocuments) throws IOException{
        for (String json: jsonDocuments) {
            loadStringAsDocument(json);
        }
    }

    private String addEscapeCharacters(String string){
        string = string.replaceAll("\"", "\\\\\"");
        return string;
    }
}
