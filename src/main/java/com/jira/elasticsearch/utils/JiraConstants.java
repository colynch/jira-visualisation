package com.jira.elasticsearch.utils;

public class JiraConstants {

    public static final String JIRA_URL = "https://devservices.jira.com/rest/api/latest/";
    public static final String PROJECT_NAME = "project";
    public static final String MAX_RESULTS = "10000";
}
