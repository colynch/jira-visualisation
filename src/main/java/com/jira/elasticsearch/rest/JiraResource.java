package com.jira.elasticsearch.rest;


import com.jira.elasticsearch.dao.ElasticsearchDao;
import com.jira.elasticsearch.dao.JiraDaoImpl;
import com.jira.elasticsearch.utils.JiraConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import java.io.IOException;

@Controller
@Path("/node")
public class JiraResource {

    @Inject
    private ElasticsearchDao elasticsearchDao;

    @Inject
    private JiraDaoImpl jiraDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(JiraResource.class);

    @GET
    @Path("/issue/{id}/{username}/{password}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getIssueFromJiraAndPublishToElasticsearch(@PathParam("id") String ticketID, @PathParam("username") String username, @PathParam("password") String password) throws IOException, JAXBException{
        try {
            String jiraTicket = jiraDao.getSingleIssueAsJson("issue/" + ticketID, username, password);
            elasticsearchDao.loadStringAsDocument(jiraTicket);
            return "Success - Issue loaded into Elasticsearch";
        } catch (Exception e){
            LOGGER.info("Exception: ", e);
            return "Unable to load Issue into Elasticsearch";
        }
    }

    @GET
    @Path("/project/{projectName}/{username}/{password}")
    @Produces(MediaType.TEXT_PLAIN)
    public String getProjectFromJiraAndPublishToElasticsearch(@PathParam("projectName") String projectName, @PathParam("username") String username, @PathParam("password") String password) throws IOException, JAXBException{
        try {
            String jql = "search?jql=project=" + projectName + "&maxResults=" + JiraConstants.MAX_RESULTS;
            //String jiraTicket = jiraDao.getProjectAsJson(jql, username, password);
            String[] tickets = jiraDao.getAllProjectIssuesAsJsonArray(jql, username, password);
            for (String ticket: tickets){
                elasticsearchDao.loadStringAsDocument(ticket);
            }
            return "Success - Issues loaded into Elasticsearch";
        } catch (Exception e){
            LOGGER.info("Exception: ", e);
            return "Unable to retrieve project information";
        }
    }


}
