package com.jira.elasticsearch.dao;

import com.jira.elasticsearch.configuration.TestConfiguration;
import com.jira.elasticsearch.service.UserServiceImpl;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.propertyeditors.InputStreamEditor;
import org.springframework.test.context.ContextConfiguration;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static com.jira.elasticsearch.utils.JiraConstants.*;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(classes = {TestConfiguration.class})
public class JiraDaoImplTest{

    @InjectMocks
    private JiraDaoImpl jiraDao;

    @InjectMocks
    private ElasticsearchDaoImpl elasticsearchDao;

    @Mock
    private UserServiceImpl userService;

    @Mock
    private HttpURLConnection connection;

    @Test
    public void testGetIssueFromJira() throws Exception{

        String issueId = "JRA-13";
        String username = "user";
        String password = "pass";
        String jsonString = "json";
        InputStream json = new ByteArrayInputStream(jsonString.getBytes());

        when(userService.buildAuthorisationString(username, password)).thenReturn("authorisationstring");
        jiraDao.getSingleIssueAsJson(issueId, username, password);
        verifyNoMoreInteractions();

    }

    @Test
    public void testGetAllIssuesFromJira() throws Exception{

        String jql = "project=DBP&maxresults=10";
        String username = "user";
        String password = "pass";
        String json  = jiraDao.getIssuesDefinedByQuery(jql, username, password);
        elasticsearchDao.loadStringAsDocument(json);
    }
}
