package com.jira.elasticsearch.configuration;

import com.jira.elasticsearch.dao.ElasticsearchDao;
import com.jira.elasticsearch.dao.ElasticsearchDaoImpl;
import com.jira.elasticsearch.dao.JiraDao;
import com.jira.elasticsearch.dao.JiraDaoImpl;
import com.jira.elasticsearch.service.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.jira.elasticsearch")
public class TestConfiguration {

    @Bean
    public JiraDaoImpl jiraDao(){return new JiraDaoImpl();}

    @Bean
    public UserServiceImpl userService(){return new UserServiceImpl();}

    @Bean
    public ElasticsearchDao elasticsearchDao(){return new ElasticsearchDaoImpl();}


}
